# GCP Workshop

## Monothlic Application
![monolith](images/monolith.png)

## [Day1 - Building a Website on Cloud Run](day1-cloudrun.md)
- [Application source code](https://github.com/googlecodelabs/monolith-to-microservices)
- Used Services
  - Cloud Build
  - Artifact Registry
  - Cloud Run

![cloudrun](images/cloudrun.png)

### [Hands-on Lab](https://www.cloudskillsboost.google/focuses/10445?parent=catalog)

## Day2 - Building  Microservices Application on GKE
![Microservices](images/microservices.png)

### [Hands-on Lab](https://www.cloudskillsboost.google/focuses/11953?parent=catalog)

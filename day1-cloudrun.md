## Day1 - Building a Website on Cloud Run
- Used Services
  - [Monolith to Microservices source code](https://github.com/googlecodelabs/monolith-to-microservices)
  - Cloud Build
  - Artifact Registry
  - Cloud Run

### Monothlic Application
![monolith](images/monolith.png)

### Cloud Build - Build and Deploy
![cloudbuild](images/cloudbuild-flow.png)

1. Use Dockerfile
- Build image and push it to Artifact Registry
```
gcloud builds submit --tag LOCATION-docker.pkg.dev/PROJECT_ID/REPOSITORY/IMAGE_NAME
```

- Building with Cloud Native Buildpacks
```
gcloud builds submit --pack builder=BUILDPACK_BUILDER, \
  env=ENVIRONMENT_VARIABLE, \
  image=IMAGE_NAME
```

- Deploy to Cloud Run:
```
gcloud run deploy --image gcr.io/PROJECT-ID/[image-name] --platform managed
```

2. Use a config file (cloudbuild.yaml)
```yaml
steps:
# Build the container image
- name: 'gcr.io/cloud-builders/docker'
  args: ['build', '-t', 'gcr.io/PROJECT_ID/IMAGE', '.']
# Push the container image to Container Registry
- name: 'gcr.io/cloud-builders/docker'
  args: ['push', 'gcr.io/PROJECT_ID/IMAGE']
# Deploy container image to Cloud Run
- name: 'gcr.io/google.com/cloudsdktool/cloud-sdk'
  entrypoint: gcloud
  args: ['run', 'deploy', 'SERVICE-NAME', '--image', 'gcr.io/PROJECT_ID/IMAGE', '--region', 'REGION']
images:
- gcr.io/PROJECT_ID/IMAGE
```

### Test Service with Docker
- Run container 
```
docker run -p 8080:8080 us-central1-docker.pkg.dev/${GOOGLE_CLOUD_PROJECT}/monolith-demo/monolith:1.0.1
```

### Cloud Run
- Managed Cloud Run: The Platform as a Service model where all container lifecycle is managed by the Cloud Run product itself. You'll be using this approach in this lab.
- Cloud Run on GKE: Cloud Run with an additional layer of control which allows you to bring your own clusters & pods from GKE.

#### Traffic Management
By default the latest revision will be assigned 100% of the inbound traffic for a service. It is possible to use "Routes" to allocate different percentages of traffic to different revisions within a service. Follow the instructions below to update your website.

- Gradually rolling out a revision
```
gcloud run deploy --image IMAGE --no-traffic
```

- Splitting traffic between multiple revisions
```
gcloud run services update-traffic SERVICE --to-revisions hello2-00005-red=25,hello2-00001-bod=25
```

- Sending all traffic to the latest revision
```
gcloud run services update-traffic SERVICE --to-latest
```

### Lab hands-on tips

#### Split Traffic

- List available revisions
```
gcloud run revisions list --region us-central1
```

-  split traffic between multiple revisions.
```
gcloud run services update-traffic monolith --region us-central1 \
--to-revisions monolith-00002-dez=50,monolith-00004-jar=50
```

- Obtain details about a given service
```
gcloud run services describe monolith --region us-central1
```

#### Benchmark Testing
- installation
```
# for *nix
apt-get install siege

# for Mac
brew install siege
```

- load test
```
# send 10 requests by 5 concurrent users to the url
siege -b -c 100 -r 10000 {Website URL}
```
